#!/bin/sh

# seems to hit a combinatorial bomb in grep if any more chars are added

$@ -e "o = \('a' w\)\('b' w\)\('c' w\)(\?){30} w = [d-z]{10,20} 10" | sort | uniq | wc -l | grep -q 3

